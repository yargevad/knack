package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	iou "io/ioutil"
	"log"
	"net/http"

	_ "github.com/lib/pq"
	nutil "github.com/yargevad/crypto/naclutil"
)

var port *int = flag.Int("port", 8080, "port to listen on")

/*
KNACK - Knowledge Negative ACKnowledgement
Message store with no insight into message contents.
*/

func dbConnect() (*sql.DB, error) {
	db, err := sql.Open("postgres", "host=127.0.0.1 dbname=knack user=knack sslmode=require")
	if err != nil {
		log.Printf("ERROR: failed to connect to database: %s\n", err)
		return nil, err
	}
	return db, nil
}

func keyExchangeHandler(w http.ResponseWriter, req *http.Request) {
	// get client's public key from Request X-Authentication header
	clientPubStr := (*req).Header.Get("X-Authentication")
	log.Printf("NOTICE: /keyx/ found client auth header [%s]\n", clientPubStr)

	// TODO: sanity check key format in a more robust way
	keyLen := len(clientPubStr)
	if keyLen < 44 {
		log.Printf("ERROR: unexpected key length %d\n", keyLen)
		http.Error(w, "Unexpected key length", http.StatusBadRequest)
		return
	}

	db, err := dbConnect()
	if err != nil {
		http.Error(w, "Database connection error", http.StatusInternalServerError)
		return
	}

	// store (unverified) public key in database
	_, err = db.Query("SELECT public.key_add($1)", clientPubStr)
	if err != nil {
		log.Printf("ERROR: failed to insert key: %s\n", err)
		http.Error(w, "Database error", http.StatusInternalServerError)
		return
	}

	// get server's public key
	// TODO: cache this in memory
	pub, _, err := nutil.FetchKeypair(".server", "storage")
	if err != nil {
		log.Printf("ERROR: failed to read server storage keys: %s\n", err)
		http.Error(w, "Couldn't read server's public key", http.StatusInternalServerError)
		return
	}
	encPub := base64.StdEncoding.EncodeToString(pub)
	log.Printf("NOTICE: sending server auth header [%s]\n", encPub)

	// send server's public key in Response X-Authentication header
	hdrs := w.Header()
	hdrs.Set("X-Authentication", encPub)
	hdrs.Set("Content-Length", "0")
	hdrs.Set("Content-Type", "text/plain")
}

func eventPutHandler(w http.ResponseWriter, req *http.Request) {
	defer req.Body.Close()

	clientPubStr := (*req).Header.Get("X-Authentication")
	log.Printf("NOTICE: /event/ found client auth header [%s]\n", clientPubStr)
	// TODO: sanity check key

	contentType := (*req).Header.Get("Content-Type")
	if contentType != "application/json" {
		log.Printf("ERROR: non-json content-type received [%s]\n", contentType)
		http.Error(w, "JSON please", http.StatusBadRequest)
		return
	}

	log.Printf("NOTICE: /event/ %s with Content-Length: %d\n", (*req).Method, (*req).ContentLength)
	// can we decode the event?
	msg := make(map[string]string)
	body, err := iou.ReadAll(req.Body)
	if err != nil {
		log.Printf("ERROR: reading request data: %s\n", err)
		http.Error(w, "Error reading request data", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &msg)

	// assert that the JSON has all the pieces we need to decrypt it
	for _, key := range []string{"to", "from", "nonce", "msg"} {
		val, ok := msg[key]
		if !ok {
			log.Printf("ERROR: found event without a '%s' key\n", key)
			http.Error(w, fmt.Sprintf("Events require a '%s' key", key), http.StatusBadRequest)
			return
		}
		log.Printf("DEBUG: [%s] = [%s]\n", key, val)
	}

	// get server key
	_, key, err := nutil.FetchKeypair(".server", "storage")
	if err != nil {
		log.Printf("ERROR: couldn't read server storage key: %s\n", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// decode hex-encoded values to bytes
	b64 := base64.StdEncoding
	decoded := make(map[string][]byte)
	for _, key := range []string{"from", "nonce", "msg"} {
		decoded[key], err = b64.DecodeString(msg[key])
		if err != nil {
			log.Printf("ERROR: base64-decoding '%s'\n", key)
			http.Error(w, fmt.Sprintf("Couldn't decode '%s'", key), http.StatusBadRequest)
			return
		}
	}

	decrypted, err := nutil.DecryptMessage(
		decoded["msg"], decoded["nonce"], decoded["from"], key)
	if err != nil {
		log.Printf("ERROR: %s\n", err)
		http.Error(w, "Couldn't decrypt message", http.StatusBadRequest)
		return
	}
	log.Printf("NOTICE: successfully decrypted message: %s\n", string(decrypted))
	// now we have a valid message

	// verify key on successful open
	db, err := dbConnect()
	if err != nil {
		http.Error(w, "Database connection error", http.StatusInternalServerError)
		return
	}

	_, err = db.Query("SELECT public.key_verify($1)", clientPubStr)
	if err != nil {
		log.Printf("ERROR: failed to verify key: %s\n", err)
		http.Error(w, "Database error", http.StatusInternalServerError)
		return
	}

	// store encrypted message in database
	_, err = db.Query("SELECT public.message_add($1, $2, $3, $4)",
		// msg contains base64-encoded values; decoded contains raw bytes
		msg["to"], msg["from"], decoded["nonce"], decoded["msg"])
	if err != nil {
		log.Printf("ERROR: failed to insert message: %s\n", err)
		http.Error(w, "Database error", http.StatusInternalServerError)
		return
	}

}

func eventGetHandler(w http.ResponseWriter, req *http.Request) {
	// TODO: decrypt message from client, to verify client has both pieces of provided key
	// TODO: retrieve message(s) from database
	// TODO: decrypt from server envelope
	// TODO: send encrypted envelope contents to client
	return
}

func eventHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "PUT":
		eventPutHandler(w, req)

	case "GET":
		eventGetHandler(w, req)

	default:
		log.Printf("ERROR: unexpected request method [%s]\n", req.Method)
		http.Error(w, "Supported methods: (PUT, GET)", http.StatusBadRequest)
		return
	}
}

func main() {
	flag.Parse()

	/* Create on startup, if not already there */
	_, _, err := nutil.FetchKeypair(".server", "storage")
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/keyx/", keyExchangeHandler)
	mux.HandleFunc("/event/", eventHandler)
	portSpec := fmt.Sprintf(":%d", *port)
	log.Fatal(http.ListenAndServe(portSpec, mux))
}
