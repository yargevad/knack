CREATE TABLE public.keys (
  key text not null,
  verified boolean not null default false,
  expiry timestamptz default clock_timestamp() + interval '1 hour'
);
CREATE UNIQUE INDEX keys_key_uidx ON public.keys (key);

CREATE TABLE public.messages (
  id bigserial not null primary key,
  to_pub text NOT NULL,
  from_pub text NOT NULL,
  nonce bytea NOT NULL,
  message bytea NOT NULL,
  expiry timestamptz default clock_timestamp() + interval '1 week'
);
CREATE INDEX messages_to_pub_idx ON public.messages (to_pub);
-- TODO: STOPPED HERE!

