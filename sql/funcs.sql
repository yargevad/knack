CREATE OR REPLACE FUNCTION public.key_add(
  in_key text
) RETURNS void LANGUAGE plpgsql STRICT AS $$
BEGIN
  INSERT INTO public.keys (key) VALUES (in_key);
EXCEPTION
WHEN unique_violation THEN
  RETURN;
END;
$$;

CREATE OR REPLACE FUNCTION public.key_verify(
  in_key text
) RETURNS void LANGUAGE sql STRICT AS $$
  UPDATE public.keys SET verified = true
   WHERE key = $1 AND verified = false;
$$;

CREATE OR REPLACE FUNCTION public.expire_unverified_keys(
) RETURNS void LANGUAGE plpgsql STRICT AS $$
DECLARE
  v_row record;
BEGIN
  FOR v_row IN
    SELECT key, expiry FROM public.keys
     WHERE verified IS false
  LOOP
    IF v_row.expiry AT TIME ZONE 'UTC' < now() AT TIME ZONE 'UTC' THEN
      RAISE NOTICE 'Expiring unverified key [%]', v_row.key;
      DELETE FROM public.keys
       WHERE verified IS false
         AND key = v_row.key;
    END IF;
  END LOOP;
END;
$$;

CREATE OR REPLACE FUNCTION public.expire_messages(
) RETURNS void LANGUAGE plpgsql STRICT AS $$
DECLARE
  v_row record;
BEGIN
  FOR v_row IN
    SELECT * FROM public.messages
     WHERE verified IS false
  LOOP
    IF v_row.expiry AT TIME ZONE 'UTC' < now() AT TIME ZONE 'UTC' THEN
      RAISE NOTICE 'Expiring message [%] => [%]: [%] [%]', v_row.to_pub, v_row.from_pub, v_row.nonce, v_row.message;
      DELETE FROM public.messages WHERE id = v_row.id;
    END IF;
  END LOOP;
END;
$$;

CREATE OR REPLACE FUNCTION public.message_add(
  in_to text, in_from text, in_nonce bytea, in_msg bytea
) RETURNS void LANGUAGE sql STRICT AS $$
  INSERT INTO public.messages (to_pub, from_pub, nonce, message)
  VALUES($1, $2, $3, $4);
$$;

-- TODO? ordering, limits
-- id can be null, to return all messages currently stored
CREATE OR REPLACE FUNCTION public.message_get(
  in_to text, in_id bigint
) RETURNS SETOF record LANGUAGE plpgsql AS $$
BEGIN
  RETURN QUERY
    -- client needs the id in order to know which messages are safe to delete
    SELECT id, from_pub, nonce, message
      FROM public.messages
     WHERE to_pub = in_to
       AND (in_id IS NULL OR in_id = id);
  RETURN;
END;
$$;

CREATE OR REPLACE FUNCTION public.message_del(
  in_to text, in_id bigint
) RETURNS void LANGUAGE sql STRICT AS $$
  DELETE FROM public.messages
   WHERE to_pub = $1
     AND id = $2;
$$;

