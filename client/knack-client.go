package main

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	flags "github.com/jessevdk/go-flags"
	_ "github.com/lib/pq"
	nutil "github.com/yargevad/crypto/naclutil"
)

const (
	DEBUG   = 0
	INFO    = 1
	NOTICE  = 2
	WARNING = 3
	ERROR   = 4
	FATAL   = 5
)

var verbosity = ERROR

var opts struct {
	From    string `short:"f" long:"from" description:"the message sender"`
	To      string `short:"t" long:"to" description:"the message recipient"`
	Mode    string `short:"m" long:"mode" description:"send or recv messages"`
	Message string `short:"g" long:"message" description:"the message to send"`
	Verbose []bool `short:"v" long:"verbose" description:"specify 1+ times for more logs"`
}

func main() {
	/* Get input from the command line. */
	_, err := flags.Parse(&opts)
	if err != nil {
		log.Fatal(err)
	}
	verbosity = verbosity - len(opts.Verbose)

	/* Sanity check input. */
	if opts.Mode != "send" && opts.Mode != "recv" {
		log.Fatalf("FATAL: mode must be 'send' or 'recv', not [%s]\n", opts.Mode)
	}
	if opts.Mode == "send" {
		if opts.From == "" {
			log.Fatal("FATAL: `from` is a required parameter with mode=send")
		}
		if opts.Message == "" {
			log.Fatal("FATAL: `message` is a required parameter with mode=send")
		}
	}
	if opts.To == "" {
		log.Fatal("FATAL: `to` is a required parameter")
	}

	if opts.Mode == "send" {
		/* Sanity check and generate sender's key if necessary. */
		fromDir := fmt.Sprintf(".%s", opts.From)
		nutil.CreateKeyStore(fromDir)
		fromPub, fromKey, err := nutil.FetchKeypair(fromDir, opts.From)
		if err != nil {
			log.Fatal(err)
		}

		/* Get server's public key */
		/* TODO: cache this somewhere */
		fromPubBase64 := base64.StdEncoding.EncodeToString(fromPub)
		client := &http.Client{}
		url := "http://linode:8080/keyx/"
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Add("X-Authentication", fromPubBase64)
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("NOTICE: %s %s\n", resp.Status, url)
		/* get X-Authentication header from response; contains server's public key */
		serverPubBase64 := resp.Header.Get("X-Authentication")
		log.Printf("NOTICE: /keyx/ sent auth header [%s]\n", serverPubBase64)
		resp.Body.Close()

		/* Key pair of sender is available, encrypt message with receiver's public key. */
		toPubPath := fmt.Sprintf(".%s/%s.pub", opts.From, opts.To)
		toPub, err := ioutil.ReadFile(toPubPath)
		if err != nil {
			log.Fatal(err)
		}

		msg, nonce, err := nutil.EncryptMessage([]byte(opts.Message), toPub, fromKey)
		if err != nil {
			log.Fatal(err)
		}

		/* build JSON, containing message encrypted for end recipient */
		toPubBase64 := base64.StdEncoding.EncodeToString(toPub)

		nonceEnc := base64.StdEncoding.EncodeToString(nonce[:])
		msgEnc := base64.StdEncoding.EncodeToString(msg)
		recipData := map[string]string{
			"to":    toPubBase64,
			"from":  fromPubBase64,
			"nonce": nonceEnc,
			"msg":   msgEnc,
		}
		/*
			This is what will be delivered to the end recipient, and can be transformed
			into cleartext in the presence of appropriate private key.
		*/
		recipJson, err := json.Marshal(recipData)
		if err != nil {
			log.Fatal(err)
		}

		/* convert received public key to binary representation */
		serverPub, err := base64.StdEncoding.DecodeString(serverPubBase64)
		if err != nil {
			log.Fatal(err)
		}
		msg, nonce, err = nutil.EncryptMessage(recipJson, serverPub, fromKey)
		if err != nil {
			log.Fatal(err)
		}

		/* build JSON, encrypted for intermediary (storage server) */
		nonceEnc = base64.StdEncoding.EncodeToString(nonce[:])
		msgEnc = base64.StdEncoding.EncodeToString(msg)
		// TODO: also send expected server key
		serverData := map[string]string{
			"to":    toPubBase64,
			"from":  fromPubBase64,
			"nonce": nonceEnc,
			"msg":   msgEnc,
		}
		/*
			This is what will be stored in the database. The "to" indicates the end
			recipient, since that will need to be visible to successfully deliver
			the message.
		*/
		serverJson, err := json.Marshal(serverData)
		if err != nil {
			log.Fatal(err)
		}

		url = "http://linode:8080/event/"
		body := bytes.NewBuffer(serverJson)
		req, err = http.NewRequest("PUT", url, body)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Add("X-Authentication", fromPubBase64)
		req.Header.Add("Content-Type", "application/json")
		resp, err = client.Do(req)
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("NOTICE: %s %s\n%s\n", resp.Status, url, respBody)

	} else if opts.Mode == "recv" {
		toDir := fmt.Sprintf(".%s", opts.To)
		nutil.CreateKeyStore(toDir)
		toPub, _, err := nutil.FetchKeypair(toDir, opts.To)
		if err != nil {
			log.Fatal(err)
		}

		/* Get server's public key */
		/* TODO: cache this somewhere */
		toPubBase64 := base64.StdEncoding.EncodeToString(toPub)
		client := &http.Client{}
		url := "http://linode:8080/keyx/"
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Add("X-Authentication", toPubBase64)
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("NOTICE: %s %s\n", resp.Status, url)
		/* get X-Authentication header from response; contains server's public key */
		serverPubBase64 := resp.Header.Get("X-Authentication")
		log.Printf("NOTICE: /keyx/ sent auth header [%s]\n", serverPubBase64)
		resp.Body.Close()

		// /* convert received public key to binary representation */
		// serverPub, err := base64.StdEncoding.DecodeString(serverPubBase64)
		// if err != nil {
		// 	log.Fatal(err)
		// }

		/* encrypt message for server, to confirm identity:
		 * some base64-encoded random data, and the current timestamp
		 */
		var binData, b64Data [nutil.NonceLength]byte
		_, err = rand.Read(binData[:])
		if err != nil {
			log.Fatal(err)
		}
		base64.StdEncoding.Encode(b64Data[:], binData[:])
		token := fmt.Sprintf("%s %s", string(b64Data[:]), time.Now().Format(time.RFC3339))
		log.Printf("NOTICE: generated token [%s]\n", token)
	}

}
