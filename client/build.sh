#!/bin/sh
go fmt knack-client.go
go get github.com/jessevdk/go-flags
go get golang.org/x/crypto/nacl/box
go get github.com/lib/pq
go build

